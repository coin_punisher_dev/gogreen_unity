﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class butterflyFollowNode : MonoBehaviour
{

    public GameObject[] PathNode;
    public GameObject Player;
    public float MoveSpeed;
    float Timer;
    int OnUpdateInSec=0;
    private Vector3 CurrentPositionHolder;
    int CurrentNode=0;
    private Vector3 startPosition;


    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
        OnUpdateInSec++;
        if(OnUpdateInSec % 3 == 0){
            if (CurrentNode < PathNode.Length - 1) {
                Timer += Time.deltaTime * MoveSpeed;
                //Debug.Log(Timer);
                startPosition = new Vector3(PathNode[CurrentNode].transform.position.x, PathNode[CurrentNode].transform.position.y, PathNode[CurrentNode].transform.position.z);
                CurrentNode++;
                CurrentPositionHolder = new Vector3(PathNode[CurrentNode].transform.position.x, PathNode[CurrentNode].transform.position.y, PathNode[CurrentNode].transform.position.z);
                
                Player.transform.position = Vector3.Lerp (startPosition, CurrentPositionHolder, Timer);
            }else{
                OnUpdateInSec=0;
                CurrentNode=0;
            }
        }
    }
}
