﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
//using GoogleMobileAds.Api;
using UnityEngine.Advertisements;
using SimpleJSON;
using System;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {

    public Animator animator_logo_size0, animator_logo_size1;
	
	public string gameId = "3436691";
    public string myPlacementId = "rewardedVideo";
    public bool testMode = false;

    public string base_url = "https://bossgame.co.id/webServices/";
    public string bossgame_email;
    public string bossgame_username;
    public string bossgame_nickname;
    public string bossgame_iduser;
    public string bossgame_idgame;
    public string bossgame_avatar;
    public int bossgame_idvalue;
    public string bossgame_valmessage;
    public string bossgame_totalpoint;

    public Text error_message_login;
    
    public GameObject bclickedsoundgame_object;
    public AudioSource bclickedsoundgame_audio_source;

    public GameObject InternetLose, HomeDialogue, InputMemberID, ParentDialogHome;

    /*public string time_first_str;
    public string time_end_str;
    public string[] TimePlaying;

    public string time_firstgp_str;
    public string time_endgp_str;
    public string[] TimePlayinggp;*/

    public InputField IFMemberID;

    public GameObject SendMemberIDButton;
	
	public bool klikPlay;

    public IEnumerator checkInternetConnection(System.Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }
	
	public IEnumerator postQuit(string iu)
    {
        WWWForm form = new WWWForm();
        form.AddField("nickname", iu);

        UnityWebRequest www = UnityWebRequest.Post(base_url + "quitGame.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
			string a = www.downloadHandler.text;
            Debug.Log(iu);
        }
    }

    IEnumerator postGetProfile(string nickname)
    {
        MainScript ms = new MainScript();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("gameID", "14");

        UnityWebRequest www = UnityWebRequest.Post(base_url + "cekNickName.php", form);
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);
        if (www.isNetworkError || www.isHttpError)
        {
			Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API Profile Login");
            string a = www.downloadHandler.text;
            Debug.Log(a);
            var jsonObject = JSON.Parse(a);
			Debug.Log(jsonObject["value"]);
			if(jsonObject["value"]==2){
			bossgame_valmessage = jsonObject["message"];
            this.klikPlay=true;
			}else{
            bossgame_iduser = jsonObject["id"];
            PlayerPrefs.SetString("bossgame_iduser", jsonObject["id"]);
            bossgame_email = jsonObject["email"];
            bossgame_username = jsonObject["username"];
            PlayerPrefs.SetString("bossgame_username", jsonObject["username"]);
            bossgame_avatar = jsonObject["photo"];
            PlayerPrefs.SetString("bossgame_avatar", jsonObject["photo"]);
            bossgame_idvalue = jsonObject["value"];
            bossgame_valmessage = jsonObject["message"];
            PlayerPrefs.SetString("bossgame_heart", jsonObject["heart"]);
            Debug.Log(bossgame_iduser + " " + bossgame_email + " " + bossgame_username);
            //UsernameBossgame.text = bossgame_username;
            if (bossgame_idvalue == 1)
            {
                PlayerPrefs.SetString("statusPlay", "1");
                HomeDialogue.SetActive(false);
                InputMemberID.SetActive(true);
                animator_logo_size1.SetInteger("logoSize",1);
            }
            Debug.Log(bossgame_iduser + bossgame_idgame);
            this.klikPlay=true;
			}
        }
    }

    public IEnumerator postPlayTimePlayingGame(string iu, string ig, string st, string et)
    {
        WWWForm form = new WWWForm();
        form.AddField("idUsers", iu);
        form.AddField("idGame", ig);
        form.AddField("startTime", st);
        form.AddField("endTime", et);

        UnityWebRequest www = UnityWebRequest.Post(base_url + "playGameEndpoint.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
        }
    }

    void OnApplicationQuit()
    {
        this.OnQuit();
    }
    private void Awake()
    {
        
        
    }
    void Start () {
		PlayerPrefs.SetString("bossgame_enemy_heart","");
        PlayerPrefs.SetString("bossgame_enemy_username","");
        PlayerPrefs.SetString("bossgame_enemy_avatar","");
        PlayerPrefs.SetString("bossgame_enemy_total_natp", "0");
        PlayerPrefs.SetString("natP_enemy", "0");
        PlayerPrefs.SetString("QAnswered_enemy", "0");
		Debug.Log(PlayerPrefs.GetString("statusAds"));
        if(PlayerPrefs.GetString("GoToMainMenu") == "Yes"){
            PlayerPrefs.SetString("statusPlay", "1");
            HomeDialogue.SetActive(false);
            InputMemberID.SetActive(true);
            GlobalVar.bannerView.Destroy();
            animator_logo_size1.SetInteger("logoSize",1);
            PlayerPrefs.SetString("GoToMainMenu", "No");
        }else{
            animator_logo_size0.SetInteger("logoSize",0);
            this.klikPlay=true;
            Advertisement.Initialize ("3436691", false);
            
            Result rs = new Result();
            MainScript ms = new MainScript();
            PlayerPrefs.SetString("statusPlay", "1");

            bossgame_nickname = PlayerPrefs.GetString("nickname");
            bossgame_idgame = "14";

            PlayerPrefs.SetString("statusPlay", "0");
            PlayerPrefs.SetString("statusAds", "0");
            PlayerPrefs.SetString("sisa_main_2_message_txt", "");
            PlayerPrefs.SetString("sisa_main_2_txt", "");
            PlayerPrefs.SetString("sisa_main_3_txt", "");
            PlayerPrefs.SetString("openPageResult", "1");
            PlayerPrefs.SetString("bossgame_iduser", "");
            PlayerPrefs.SetString("bossgame_idgame", "14");
            PlayerPrefs.SetString("bossgame_heart", "0");
            PlayerPrefs.SetString("bossgame_total_natp", "0");
            PlayerPrefs.SetString("natP", "0");
            PlayerPrefs.SetString("QAnswered", "0");


            //StartCoroutine(postGetProfile(bossgame_nickname));
            PlayerPrefs.SetString("time_first_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            PlayerPrefs.SetString("time_end_str", "");
            PlayerPrefs.SetString("TimePlaying1", PlayerPrefs.GetString("time_first_str"));
            PlayerPrefs.SetString("TimePlaying2", PlayerPrefs.GetString("time_first_str"));

            PlayerPrefs.SetString("time_firstgp_str", "");
            PlayerPrefs.SetString("time_endgp_str", "");
            PlayerPrefs.SetString("TimePlayinggp1", PlayerPrefs.GetString("time_firstgp_str"));
            PlayerPrefs.SetString("TimePlayinggp2", PlayerPrefs.GetString("time_firstgp_str"));
        }
        


        /*if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = false;
            bclickedsoundgame_audio_source.Stop();
        }*/
        StartCoroutine(checkInternetConnection((isConnected) => {
            if (!isConnected)
            {
                InternetLose.SetActive(true);
                HomeDialogue.SetActive(false);
                ParentDialogHome.SetActive(false);
            }
        }));
        

    }

    void Update()
    {
        //PlayerPrefs.SetString("nickname", "932748");
		if (Input.GetKeyDown(KeyCode.Escape)) { 
			this.OnQuit();
		}
		
		if(bossgame_valmessage =="Success"){
			bossgame_valmessage = "";
		}
		
        if (bossgame_valmessage !="")
        {
            error_message_login.text = bossgame_valmessage;
        }
        else
        {
            error_message_login.text = "";
        }
    }
    
    public void OnQuit()
    {
		Debug.Log(PlayerPrefs.GetString("nickname"));
		StartCoroutine(postQuit(PlayerPrefs.GetString("nickname")));
        PlayerPrefs.SetString("time_end_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        PlayerPrefs.SetString("TimePlaying1", PlayerPrefs.GetString("time_first_str"));
        PlayerPrefs.SetString("TimePlaying2", PlayerPrefs.GetString("time_end_str"));
        StartCoroutine(postPlayTimePlayingGame(bossgame_iduser, bossgame_idgame, PlayerPrefs.GetString("TimePlaying1"), PlayerPrefs.GetString("TimePlaying2")));
        Application.Quit();
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
    }

    public void OnBackToHome()
    {
        Result rs = new Result();

        InputMemberID.SetActive(false);
        HomeDialogue.SetActive(true);
        animator_logo_size0.SetInteger("logoSize",0);
        PlayerPrefs.SetString("statusAds", "0");
        SceneManager.LoadScene(0);
        if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
    }

    public void SinglePlayer()
    {
        Result rs = new Result();
        PlayerPrefs.SetString("statusAds", "0");
        PlayerPrefs.SetString("modePlay", "singleplayer");
        SceneManager.LoadScene(1);
        if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
    }

    public void MultiPlayer()
    {
        Result rs = new Result();
        PlayerPrefs.SetString("statusAds", "0");
        PlayerPrefs.SetString("modePlay", "multiplayer");
        SceneManager.LoadScene(4);
        if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
    }

    public void cancelToMainMenu()
    {
        Result rs = new Result();
        PlayerPrefs.SetString("statusAds", "0");
        animator_logo_size0.SetInteger("logoSize",0);
        HomeDialogue.SetActive(true);
        InputMemberID.SetActive(false);
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
    }

    public void findMemberID()
    {
		if(klikPlay){
            this.klikPlay=false;
			PlayerPrefs.SetString("nickname", IFMemberID.text.ToString());
			StartCoroutine(postGetProfile(IFMemberID.text.ToString()));
            
		}
		//int heartMinus = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
        //PlayerPrefs.SetString("bossgame_heart", heartMinus.ToString());
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }

    }

}
