﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using GoogleMobileAds.Api;
using System;

public class LoadProfileAndHealth : MonoBehaviour
{
    public RawImage avatar2;
    public Text username;
    public Text heart;
    IEnumerator LoadImgAvatar(string url)
    {
        yield return null;
        WWW www = new WWW(url);
        yield return www;

        displayImageAvatar(www.texture);
    }

    void displayImageAvatar(Texture2D imgToDisp)
    {
        avatar2.GetComponent<RectTransform>().sizeDelta = new Vector2(51, 51);
        avatar2.texture = imgToDisp;
    }
	
    /// <summary>
    /// OnGUI is called for rendering and handling GUI events.
    /// This function can be called multiple times per frame (one call per event).
    /// </summary>
    void OnGUI(){
        Login Udata = new Login();
        username.text = PlayerPrefs.GetString("bossgame_username");
        heart.text = PlayerPrefs.GetString("bossgame_heart"); 
        StartCoroutine(LoadImgAvatar(Udata.base_url.ToString() + "profile/" + PlayerPrefs.GetString("bossgame_avatar").ToString()));
    }
    

}
