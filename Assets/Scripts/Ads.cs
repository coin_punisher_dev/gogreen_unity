﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using SimpleJSON;
using System;

public class Ads : MonoBehaviour {
	
	/*public string gameId = "3436691";
    public string myPlacementId = "rewardedVideo";
    public bool testMode = false;*/
	
	IEnumerator WaitingAdsTimeout()
    {
        //Print the time of when the function is first called.
		PlayerPrefs.SetString("statusAds","0");
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(14);

		if(PlayerPrefs.GetString("modePlay")=="singleplayer"){
			SceneManager.LoadScene(1);
		} else if(PlayerPrefs.GetString("modePlay")=="multiplayer"){
			SceneManager.LoadScene(4);
		}
		

        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }
	
	void Start()
    {
		Debug.Log("hasil nyawa : "+PlayerPrefs.GetString("bossgame_heart")+" Value: "+PlayerPrefs.GetInt("bossgame_value"));
		//int heartMinus = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
		//PlayerPrefs.SetString("bossgame_heart", heartMinus.ToString());
		/*Debug.Log(PlayerPrefs.GetString("statusAds"));
        Advertisement.Initialize (gameId, testMode);*/
	}
	
	void Update(){
			Login lg = new Login();
			Debug.Log("Test");
			if (PlayerPrefs.GetString("statusAds") == "1")
			{
				Debug.Log("ads1");
				if(Advertisement.IsReady(lg.myPlacementId)){
					PlayerPrefs.SetString("statusAds","0");
					Debug.Log("ads2");
						Advertisement.Show(lg.myPlacementId, new ShowOptions
						{
							resultCallback = result => {
								Debug.Log(result.ToString());
								if(result.ToString()=="Finished"){
									AdFinished();
								} else if(result.ToString()=="Skipped"){
									AdSkipped();
								} else if(result.ToString()=="Failed"){
									AdFailed();
								}
							}
						});
				}
				StartCoroutine(WaitingAdsTimeout());
			}
		
	}
	
	
	
	
	void AdFinished() {
			PlayerPrefs.SetString("statusAds","0");
            Debug.Log("User Watching Ads");
			if(PlayerPrefs.GetInt("bossgame_value") == 2){
				SceneManager.LoadScene(0);
			}else{
				Debug.Log("User Back Play");
				MainScript ms = new MainScript();
				StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "OnUnityAdsDidFinish:Finished", 1, 0.0));
				StartCoroutine(WaitingAdsTimeout());
			}
        
    }
	
	void AdSkipped() {
			PlayerPrefs.SetString("statusAds","0");
            Debug.Log("User Skip Ads");
			if(PlayerPrefs.GetInt("bossgame_value") == 2){
				SceneManager.LoadScene(0);
			}else{
				Debug.Log("User Back Play");
				MainScript ms = new MainScript();
				StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "OnUnityAdsDidFinish:Skipped", 2, 0.0));
				StartCoroutine(WaitingAdsTimeout());
			}
        
    }
	
	void AdFailed() {
			PlayerPrefs.SetString("statusAds","0");
            Debug.Log("User Failed Load Ads");
			if(PlayerPrefs.GetInt("bossgame_value") == 2){
				SceneManager.LoadScene(0);
			}else{
				Debug.Log("User Back Play");
				MainScript ms = new MainScript();
				StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "OnUnityAdsDidFinish:Failed", 3, 0.0));
				StartCoroutine(WaitingAdsTimeout());
			}
        
    }
}
