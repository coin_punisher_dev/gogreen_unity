﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
//using GoogleMobileAds.Api;
//using UnityEngine.Advertisements;
using SimpleJSON;
using System;
using UnityEngine.SceneManagement;

public class Result : MonoBehaviour
{
    
    public GameObject InternetLose, LoseDialogue, WinDialogue;
    public Text sisa_main_2_message_go, sisa_main_2_go, sisa_main_3_go, naturepoint, questionanswered, naturepoint2, questionanswered2;

    public GameObject bclickedsoundgame_object;
    public AudioSource bclickedsoundgame_audio_source;

    public GameObject soundmenanggame_object;
    public AudioSource soundmenanggame_audio_source;
    public GameObject soundkalahgame_object;
    public AudioSource soundkalahgame_audio_source;
	
	public bool klikPlay;
	
	IEnumerator postGetProfile(string nickname)
    {
		Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("gameID", 14);

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "cekNickName.php", form);
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);
        if (www.isNetworkError || www.isHttpError)
        {
			Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API Profile Login");
            string a = www.downloadHandler.text;
            Debug.Log(a);
            var jsonObject = JSON.Parse(a);
			Debug.Log(jsonObject["value"]);
			if(jsonObject["value"]==2){
			SceneManager.LoadScene(0);
			PlayerPrefs.SetInt("bossgame_value", jsonObject["value"]);
			}else{
            
            if (jsonObject["value"] == 1)
            {
                PlayerPrefs.SetString("statusPlay", "1");
				PlayerPrefs.SetString("bossgame_heart", jsonObject["heart"]);
				PlayerPrefs.SetInt("bossgame_value", jsonObject["value"]);
				Debug.Log("hasil nyawa : "+PlayerPrefs.GetString("bossgame_heart"));
				SceneManager.LoadScene(3);
            }
			}
        }
    }
	
	public IEnumerator checkInternetConnection(System.Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }

    public void OnExit()
    {
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
        Debug.Log(PlayerPrefs.GetString("nickname"));
        StartCoroutine(postQuit(PlayerPrefs.GetString("nickname")));
        PlayerPrefs.SetString("time_end_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        PlayerPrefs.SetString("TimePlaying1", PlayerPrefs.GetString("time_first_str"));
        PlayerPrefs.SetString("TimePlaying2", PlayerPrefs.GetString("time_end_str"));
        StartCoroutine(postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), PlayerPrefs.GetString("bossgame_idgame"), PlayerPrefs.GetString("TimePlaying1"), PlayerPrefs.GetString("TimePlaying2")));
        Application.Quit();
    }
/*
    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleRewardBasedVideoLoaded", 1, 0.0));
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }
    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleRewardBasedVideoFailedToLoad", 2, 0.0));
    }
    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleRewardBasedVideoOpened", 3, 0.0));
    }
    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleRewardBasedVideoStarted", 4, 0.0));
    }
    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleRewardBasedVideoClosed", 5, 0.0));
    }
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);

        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleRewardBasedVideoRewarded", 6, amount));
    }
    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");

        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleRewardBasedVideoLeftApplication", 7, 0.0));
    }
*/
	
    void Start()
    {
		this.klikPlay = true;
        Login lg = new Login();
        MainScript ms = new MainScript();
        Debug.Log("test open " + PlayerPrefs.GetString("openPageResult"));
        Debug.Log("test open 0/1");
		
		
        // Called when an ad request has successfully loaded.
        //GlobalVar.reward_based_ad.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        //GlobalVar.reward_based_ad.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        //GlobalVar.reward_based_ad.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        //GlobalVar.reward_based_ad.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        //GlobalVar.reward_based_ad.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        //GlobalVar.reward_based_ad.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        //GlobalVar.reward_based_ad.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
		

        Debug.Log("Test Score " + PlayerPrefs.GetString("natP") + " " + PlayerPrefs.GetString("QAnswered"));
        if (PlayerPrefs.GetString("openPageResult") == "0")
        {
            WinDialogue.SetActive(true);
            InternetLose.SetActive(false);
            LoseDialogue.SetActive(false);
            PlayerPrefs.SetString("statusAds", "0");
            PlayerPrefs.SetString("time_endgp_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            PlayerPrefs.SetString("TimePlayinggp1", PlayerPrefs.GetString("time_firstgp_str"));
            PlayerPrefs.SetString("TimePlayinggp2", PlayerPrefs.GetString("time_endgp_str"));
            StartCoroutine(ms.postNaturePointGame(PlayerPrefs.GetString("bossgame_iduser"), PlayerPrefs.GetString("bossgame_idgame"), PlayerPrefs.GetString("TimePlayinggp1"), PlayerPrefs.GetString("TimePlayinggp2"), PlayerPrefs.GetString("natP")));
            naturepoint2.text = PlayerPrefs.GetString("natP");
			int jumlahpoint = int.Parse(PlayerPrefs.GetString("bossgame_total_natp")) + int.Parse(PlayerPrefs.GetString("natP"));
			PlayerPrefs.SetString("bossgame_total_natp", jumlahpoint.ToString());
            questionanswered2.text = PlayerPrefs.GetString("QAnswered");
            sisa_main_3_go.text = PlayerPrefs.GetString("bossgame_heart");
            Debug.Log("test open 0");
            soundmenanggame_audio_source = soundmenanggame_object.GetComponent<AudioSource>();
            soundmenanggame_audio_source.GetComponent<AudioSource>().enabled = true;
            soundmenanggame_audio_source.Play();
        }
        else
        {
            WinDialogue.SetActive(false);
            InternetLose.SetActive(false);
            LoseDialogue.SetActive(true);

            //StartCoroutine(ms.postChancePlaying(PlayerPrefs.GetString("nickname"), "Go Green"));
            //int heartMinus = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
            //PlayerPrefs.SetString("bossgame_heart", heartMinus.ToString());
            PlayerPrefs.SetString("statusAds", "1");
            PlayerPrefs.SetString("time_endgp_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            PlayerPrefs.SetString("TimePlayinggp1", PlayerPrefs.GetString("time_firstgp_str"));
            PlayerPrefs.SetString("TimePlayinggp2", PlayerPrefs.GetString("time_endgp_str"));
            StartCoroutine(ms.postNaturePointGame(PlayerPrefs.GetString("bossgame_iduser"), PlayerPrefs.GetString("bossgame_idgame"), PlayerPrefs.GetString("TimePlayinggp1"), PlayerPrefs.GetString("TimePlayinggp2"), PlayerPrefs.GetString("natP")));
            naturepoint.text = PlayerPrefs.GetString("natP");
            questionanswered.text = PlayerPrefs.GetString("QAnswered");
            sisa_main_2_go.text = PlayerPrefs.GetString("bossgame_heart");
            sisa_main_2_message_go.text = PlayerPrefs.GetString("sisa_main_2_message_txt");
            Debug.Log("test open 1");
            if (!soundkalahgame_audio_source.isPlaying)
            {
                soundkalahgame_audio_source = soundkalahgame_object.GetComponent<AudioSource>();
                soundkalahgame_audio_source.GetComponent<AudioSource>().enabled = true;
                soundkalahgame_audio_source.Play();
            }
        }

        if (!soundmenanggame_audio_source.isPlaying)
        {
            soundmenanggame_audio_source = soundmenanggame_object.GetComponent<AudioSource>();
            soundmenanggame_audio_source.GetComponent<AudioSource>().enabled = false;
            soundmenanggame_audio_source.Stop();
        }

        if (!soundkalahgame_audio_source.isPlaying)
        {
            soundkalahgame_audio_source = soundkalahgame_object.GetComponent<AudioSource>();
            soundkalahgame_audio_source.GetComponent<AudioSource>().enabled = false;
            soundkalahgame_audio_source.Stop();
        }
        StartCoroutine(checkInternetConnection((isConnected) => {
            if (!isConnected)
            {
                InternetLose.SetActive(true);
                WinDialogue.SetActive(false);
                LoseDialogue.SetActive(false);
            }
        }));

        

    }
    public IEnumerator postQuit(string iu)
    {
        Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", iu);

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "quitGame.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
            string a = www.downloadHandler.text;
            Debug.Log(iu);
        }
    }

    public IEnumerator postPlayTimePlayingGame(string iu, string ig, string st, string et)
    {
        Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("idUsers", iu);
        form.AddField("idGame", ig);
        form.AddField("startTime", st);
        form.AddField("endTime", et);

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "playGameEndpoint.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
        }
    }
	
	/*private void HandleShowResult(ShowResult result)
    {
		if (result == ShowResult.Finished) {
			switch (result)
			{
				case ShowResult.Finished:
					Debug.Log("The ad was successfully shown.");
					AdFinished();
					break;
	 
				case ShowResult.Skipped:
					Debug.Log("The ad was skipped before reaching the end.");
					break;
	 
				case ShowResult.Failed:
					Debug.LogError("The ad failed to be shown.");
					break;
			}
		}
    }*/

    void Update()
    {
        //PlayerPrefs.SetString("nickname", "932748");
        Login lg = new Login();
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log(PlayerPrefs.GetString("nickname"));
            StartCoroutine(postQuit(PlayerPrefs.GetString("nickname")));
            PlayerPrefs.SetString("time_end_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            PlayerPrefs.SetString("TimePlaying1", PlayerPrefs.GetString("time_first_str"));
            PlayerPrefs.SetString("TimePlaying2", PlayerPrefs.GetString("time_end_str"));
            StartCoroutine(postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), PlayerPrefs.GetString("bossgame_idgame"), PlayerPrefs.GetString("TimePlaying1"), PlayerPrefs.GetString("TimePlaying2")));
            Application.Quit();
        }
    }
    
    public void BackToHome2()
    {
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
        MainScript ms = new MainScript();
        PlayerPrefs.SetString("statusGoNext", "1");
		StartCoroutine(postQuit(PlayerPrefs.GetString("nickname")));
        PlayerPrefs.SetString("time_end_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        PlayerPrefs.SetString("TimePlaying1", PlayerPrefs.GetString("time_first_str"));
        PlayerPrefs.SetString("TimePlaying2", PlayerPrefs.GetString("time_end_str"));
        StartCoroutine(postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), PlayerPrefs.GetString("bossgame_idgame"), PlayerPrefs.GetString("TimePlaying1"), PlayerPrefs.GetString("TimePlaying2")));
        Application.Quit();
    }

    public void OnSinglePlayerClicked2()
    {
		
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
		if(klikPlay){
			if (PlayerPrefs.GetString("statusAds") == "1")
			{
				//MainScript ms = new MainScript();
				//StartCoroutine(ms.postChancePlaying(PlayerPrefs.GetString("nickname"), "Go Green"));
				//int heartMinus = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
				//PlayerPrefs.SetString("bossgame_heart", heartMinus.ToString());
				Login lg = new Login();
				StartCoroutine(lg.postQuit(PlayerPrefs.GetString("nickname")));
				StartCoroutine(postGetProfile(PlayerPrefs.GetString("nickname")));
			}else{
				SceneManager.LoadScene(1);
			}
		}
		this.klikPlay = false;
    }


    void OnGUI()
    {
		
        /*if (GlobalVar.reward_based_ad.IsLoaded())
        {
            if (PlayerPrefs.GetString("statusAds") == "1")
            {
                Debug.Log("show iklan");
                GlobalVar.reward_based_ad.Show();
            }
        }*/
    }
}
