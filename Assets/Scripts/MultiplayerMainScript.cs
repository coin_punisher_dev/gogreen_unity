﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using GoogleMobileAds.Api;
using UnityEngine.Advertisements;
using SimpleJSON;
using System;
using UnityEngine.SceneManagement;

public class MultiplayerMainScript : MonoBehaviour {

    public GameObject GamePlay;
    public GameObject LoadingUserPage;
    bool showGamePlay = false;
    int showGamePlayCount = 0;
	
	public Image answerBox1;
    public Image answerBox2;
    public Image answerBox3;
    public Image answerBox4;
    public Image answerBox5;
    public Image answerBox6;
    public Image answerBox7;
    public Image answerBox8;
    public Image answerBox9;

    public Text answerTextBox1;
    public Text answerTextBox2;
    public Text answerTextBox3;
    public Text answerTextBox4;
    public Text answerTextBox5;
    public Text answerTextBox6;
    public Text answerTextBox7;
    public Text answerTextBox8;
    public Text answerTextBox9;

    public RawImage avatar;
    public RawImage avatar2;

    public Text sisa_main, QuestionText, DashesText, TotalnaturepointBossgame2, UsernameBossgame2;
    public Image HangmanImage;
    public Sprite[] HangmanSprites;
    public Sprite avatarBossgame;
    public Sprite WinSprite, LoseSprite;
    public int natP = 0;
    public int Qanswered = 0;
    public GameObject bsgame_object;
    public GameObject bclickedsoundgame_object;
    public AudioSource bsgame_audio_source;
    public AudioSource bclickedsoundgame_audio_source;

    public GameObject jawabansalahsoundgame_object;
    public AudioSource jawabansalahsoundgame_audio_source;
    public GameObject jawabanbenarsoundgame_object;
    public AudioSource jawabanbenarsoundgame_audio_source;

    public GameObject InternetLose, MainDialogue;

    public GameObject ab1, ab2, ab3, ab4, ab5, ab6, ab7, ab8, ab9;

    private int currentQuest = 0;
    private int currentHangmanSprite = 0;
    private const int TOTAL_HANGMAN_SPRITES = 8;
    private const char PLACEHOLDER = '*';

    private Dictionary<string, string> gameDict;
    private string answer, userInput;
    public int[] randomizeQuestion;


    public IEnumerator checkInternetConnection(System.Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }

    public IEnumerator getTotalPointBossgame(string iu, string ig)
    {
        Login lg = new Login();
        UnityWebRequest www = UnityWebRequest.Get(lg.base_url + "getTotalPointGame.php?idUsers="+iu+"&idGame="+ig);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            //Debug.Log(www.error);
        }
        else
        {
            //Debug.Log("Success Send API");
            string a = www.downloadHandler.text;
            var jsonObject = JSON.Parse(a);
            lg.bossgame_totalpoint = jsonObject[0]["totalPoint"];
            //Debug.Log(lg.bossgame_totalpoint);
            //TotalnaturepointBossgame.text = "Your Nature Point : "+bossgame_totalpoint;
		    PlayerPrefs.SetString("bossgame_total_natp",jsonObject[0]["totalPoint"]);
            TotalnaturepointBossgame2.text = "Your Nature Point : " + PlayerPrefs.GetString("bossgame_total_natp");
        }
    }

    public IEnumerator LoadImgAvatar(string url)
    {
        yield return null;
        WWW www = new WWW(url);
        yield return www;

        displayImageAvatar(www.texture);
    }

    public IEnumerator postQuit(string iu)
    {
        Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", iu);

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "quitGame.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            //Debug.Log(www.error);
        }
        else
        {
            //Debug.Log("Success Send API");
			string a = www.downloadHandler.text;
            //Debug.Log(iu);
        }
    }

    public string ConvertArrayToStringJoin(int[] array) {
        string result = string.Join(".", array);
        return result;
    }

    public void ranQ(bool target=true)
    {
        Login lg = new Login();
        if (target)
        {
            PlayerPrefs.SetString("statusPlay", "1");
            gameDict = new Dictionary<string, string>();
            LoadDictionary("GoGreenDictionary", gameDict);
            StartCoroutine(LoadImgAvatar(lg.base_url + "profile/" + lg.bossgame_avatar));
            UsernameBossgame2.text = PlayerPrefs.GetString("bossgame_username");
            sisa_main.text = PlayerPrefs.GetString("bossgame_heart");
            StartCoroutine(getTotalPointBossgame(PlayerPrefs.GetString("bossgame_iduser"), PlayerPrefs.GetString("bossgame_idgame")));
            randomizeQuestion = new int[gameDict.Count];
            System.Random randNum = new System.Random();
            for (int i = 0; i < randomizeQuestion.Length; i++)
            {
                randomizeQuestion[i] = randNum.Next(0, gameDict.Count);
            }
            MainDialogue.SetActive(true);
            currentHangmanSprite = 0;
            currentQuest = 0;

            HangmanImage.sprite = Resources.Load<Sprite>("Sprites/" + currentHangmanSprite.ToString());
            int randInt = randomizeQuestion[currentQuest];
            //int randInt = Random.Range(0, gameDict.Count);
            QuestionText.text = gameDict.ElementAt(randInt).Key;
            answer = gameDict.ElementAt(randInt).Value.ToUpper();
            StringBuilder sb = new StringBuilder("");
            ab1.SetActive(false);
            ab2.SetActive(false);
            ab3.SetActive(false);
            ab4.SetActive(false);
            ab5.SetActive(false);
            ab6.SetActive(false);
            ab7.SetActive(false);
            ab8.SetActive(false);
            ab9.SetActive(false);
            for (int i = 0; i < answer.Length; i++)
            {
                if (i % 3 == 0)
                {
                    sb.Append(answer[i]);
                    switch (i)
                    {
                        case 0:
                            ab1.SetActive(true);
                            answerTextBox1.text = answer[i].ToString();
                            break;
                        case 1:
                            ab2.SetActive(true);
                            answerTextBox2.text = answer[i].ToString();
                            break;
                        case 2:
                            ab3.SetActive(true);
                            answerTextBox3.text = answer[i].ToString();
                            break;
                        case 3:
                            ab4.SetActive(true);
                            answerTextBox4.text = answer[i].ToString();
                            break;
                        case 4:
                            ab5.SetActive(true);
                            answerTextBox5.text = answer[i].ToString();
                            break;
                        case 5:
                            ab6.SetActive(true);
                            answerTextBox6.text = answer[i].ToString();
                            break;
                        case 6:
                            ab7.SetActive(true);
                            answerTextBox7.text = answer[i].ToString();
                            break;
                        case 7:
                            ab8.SetActive(true);
                            answerTextBox8.text = answer[i].ToString();
                            break;
                        case 8:
                            ab9.SetActive(true);
                            answerTextBox9.text = answer[i].ToString();
                            break;
                    }
                }
                else
                {
                    sb.Append(PLACEHOLDER);
                    switch (i)
                    {
                        case 0:
                            ab1.SetActive(true);
                            answerTextBox1.text = PLACEHOLDER.ToString();
                            break;
                        case 1:
                            ab2.SetActive(true);
                            answerTextBox2.text = PLACEHOLDER.ToString();
                            break;
                        case 2:
                            ab3.SetActive(true);
                            answerTextBox3.text = PLACEHOLDER.ToString();
                            break;
                        case 3:
                            ab4.SetActive(true);
                            answerTextBox4.text = PLACEHOLDER.ToString();
                            break;
                        case 4:
                            ab5.SetActive(true);
                            answerTextBox5.text = PLACEHOLDER.ToString();
                            break;
                        case 5:
                            ab6.SetActive(true);
                            answerTextBox6.text = PLACEHOLDER.ToString();
                            break;
                        case 6:
                            ab7.SetActive(true);
                            answerTextBox7.text = PLACEHOLDER.ToString();
                            break;
                        case 7:
                            ab8.SetActive(true);
                            answerTextBox8.text = PLACEHOLDER.ToString();
                            break;
                        case 8:
                            ab9.SetActive(true);
                            answerTextBox9.text = PLACEHOLDER.ToString();
                            break;
                    }
                }
            }
            DashesText.text = sb.ToString();
            userInput = sb.ToString();

            Debug.Log(ConvertArrayToStringJoin(randomizeQuestion) + " / "+sb.ToString());
            PlayerPrefs.SetString("time_firstgp_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            PlayerPrefs.SetString("time_endgp_str", "");
            PlayerPrefs.SetString("TimePlayinggp1", PlayerPrefs.GetString("time_firstgp_str"));
            PlayerPrefs.SetString("TimePlayinggp2", PlayerPrefs.GetString("time_firstgp_str"));
        }
        else
        {

        }
    }

    public IEnumerator postChancePlaying(string nickname, string game_name)
    {
        Result rs = new Result();
        Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("namaGame", "Go Green");

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "lifeMinusGame.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            //Debug.Log(www.error);
        }
        else
        {
            PlayerPrefs.SetString("sisa_main_2_message_txt", "");
            PlayerPrefs.SetString("sisa_main_2_txt", "");
            PlayerPrefs.SetString("sisa_main_3_txt", "");
            //Debug.Log("Success Send API");
            string a = www.downloadHandler.text;
            var jsonObject = JSON.Parse(a);
            //Debug.Log(jsonObject["value"]+" - "+ jsonObject["message"]);
            if (jsonObject["value"] == 0)
            {
                this.OnBackToHome();
                PlayerPrefs.SetString("statusPlay", "0");
                PlayerPrefs.SetString("sisa_main_2_message_txt", "");
            }
            else if (jsonObject["value"] == 1)
            {
                if (jsonObject["heart"] <= 0) {
                    PlayerPrefs.SetString("sisa_main_2_txt", "0");
                    PlayerPrefs.SetString("sisa_main_2_message_txt", "Kesempatan Bermain Telah Habis");
                    PlayerPrefs.SetString("statusPlay", "0");
                }
                else
                {
                    PlayerPrefs.SetString("sisa_main_2_txt", jsonObject["heart"]);
                    PlayerPrefs.SetString("sisa_main_2_message_txt", "");
                    PlayerPrefs.SetString("sisa_main_3_txt", jsonObject["heart"]);
                    PlayerPrefs.SetString("statusPlay", "1");
                }
            }
            else if (jsonObject["value"] == 2)
            {
                PlayerPrefs.SetString("sisa_main_2_txt", jsonObject["heart"]);
                PlayerPrefs.SetString("sisa_main_2_message_txt", jsonObject["message"]);
                PlayerPrefs.SetString("statusPlay", "0");
            }

        }
    }

    public IEnumerator postPlayTimePlayingGame(string iu, string ig, string st, string et)
    {
        Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("idUsers", iu);
        form.AddField("idGame", ig);
        form.AddField("startTime", st);
        form.AddField("endTime", et);

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "playGameEndpoint.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            //Debug.Log(www.error);
        }
        else
        {
            //Debug.Log("Success Send API");
        }
    }

    public IEnumerator postNaturePointGame(string iu, string ig, string sg, string eg, string np)
    {
        ////Debug.Log(iu+" "+ig+" "+sg+" "+eg+" "+np);
        Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("idUsers", iu);
        form.AddField("idGame", ig);
        form.AddField("startGame", sg);
        form.AddField("endGame", eg);
        form.AddField("point", np);

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "pushGamePoint.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            //Debug.Log(www.error);
        }
        else
        {
            //Debug.Log("Success Send API");
        }
    }

    public IEnumerator PostRunningAds(string _param, string _message, int _ads, double _amount)
    {
        WWWForm form = new WWWForm();
        Login lg = new Login();
        form.AddField("memberID", _param);
        form.AddField("gameID", "14");
        form.AddField("message", _message);
        form.AddField("ads", _ads);
        form.AddField("amount", _amount.ToString());

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "runningAds.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            //Debug.Log(www.error);
        }
        else
        {
            //Debug.Log(www.downloadHandler.text);
        }

    }

    void OnApplicationQuit()
    {
        //Debug.Log(PlayerPrefs.GetString("nickname"));
        StartCoroutine(postQuit(PlayerPrefs.GetString("nickname")));
        PlayerPrefs.SetString("time_end_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        PlayerPrefs.SetString("TimePlaying1", PlayerPrefs.GetString("time_first_str"));
        PlayerPrefs.SetString("TimePlaying2", PlayerPrefs.GetString("time_end_str"));
        StartCoroutine(postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), PlayerPrefs.GetString("bossgame_idgame"), PlayerPrefs.GetString("TimePlaying1"), PlayerPrefs.GetString("TimePlaying2")));
        Application.Quit();
    }

    void OnExit() 
    {
        //Debug.Log(PlayerPrefs.GetString("nickname"));
        StartCoroutine(postQuit(PlayerPrefs.GetString("nickname")));
        PlayerPrefs.SetString("time_end_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        PlayerPrefs.SetString("TimePlaying1", PlayerPrefs.GetString("time_first_str"));
        PlayerPrefs.SetString("TimePlaying2", PlayerPrefs.GetString("time_end_str"));
        StartCoroutine(postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), PlayerPrefs.GetString("bossgame_idgame"), PlayerPrefs.GetString("TimePlaying1"), PlayerPrefs.GetString("TimePlaying2")));
        Application.Quit();
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
    }
/*
    void InitGoogleAd()
    {
        // #if UNITY_ANDROID
        // 	string app_id = "ca-app-pub-3940256099942544~3347511713";
        // #elif UNITY_IPHONE
        // 	string app_id = "ca-app-pub-3940256099942544~1458002511";
        // #else
        // 	string app_id = "unexpected_platform";
        // #endif

        //production app id
        //string app_id = "ca-app-pub-2705296754237795~8902070495";

        //sample app id
        string app_id = "ca-app-pub-3940256099942544~3347511713";

        MobileAds.Initialize(app_id);

        GlobalVar.reward_based_ad = RewardBasedVideoAd.Instance;

        this.RequestRewardBasedVideo();

    }
    private void RequestRewardBasedVideo()
    {
        // #if UNITY_ANDROID
        // 	string ad_unit_id = "ca-app-pub-3940256099942544/5224354917";
        // #elif UNITY_IPHONE
        // 	string ad_unit_id = "ca-app-pub-3940256099942544/1712485313";
        // #else
        // 	string ad_unit_id = "unexpected_platform";
        // #endif

        //production unit id
        //string ad_unit_id = "ca-app-pub-2705296754237795/1566505954";

        //sample unit id
        string ad_unit_id = "ca-app-pub-3940256099942544/5224354917";

        AdRequest request = new AdRequest.Builder().Build();

        GlobalVar.reward_based_ad.LoadAd(request, ad_unit_id);
    }
*/

    IEnumerator loadAvatarEnemy(string ph){
        yield return null;
        /*WWW www2 = new WWW("https://i.dlpng.com/static/png/6728146_preview.png");
        yield return www2;
        Texture texture2 = www2.texture;*/
        
        //GameObject.Find("avatar2Enemy").GetComponent<RawImage>().texture = texture2;
        Debug.Log(PlayerPrefs.GetString("bossgame_enemy_avatar"));
        WWW www3 = new WWW(ph);
        yield return www3;
        Texture texture3 = www3.texture;
        GameObject.Find("avatar2Enemy").GetComponent<RectTransform>().sizeDelta = new Vector2(51, 51);
        GameObject.Find("avatar2Enemy").GetComponent<RawImage>().texture = texture3;
        showGamePlay=true;
    }

    public static void LoadEnemyProfile(string ur,string np,string ht){
        GameObject.Find("UsernameBossgame2Enemy").GetComponent<Text>().text=ur;
        GameObject.Find("TotalnaturepointBossgame2Enemy").GetComponent<Text>().text="User Nature Point:"+np;
        GameObject.Find("sisa_mainEnemy").GetComponent<Text>().text=ht;
    }
	
    void Start () {

        //InitGoogleAd();

        showGamePlay=false;

        LoadingUserPage.SetActive(true);
        GamePlay.SetActive(false);
		
        Login lg = new Login();
        Result rs = new Result();
		//Debug.Log(PlayerPrefs.GetString("statusAds"));
        Advertisement.Initialize ("3436691", true);
		GoogleAds gads = new GoogleAds();
		gads.startBanner();
        PlayerPrefs.SetString("time_firstgp_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        PlayerPrefs.SetString("time_endgp_str", "");
        PlayerPrefs.SetString("TimePlayinggp1", PlayerPrefs.GetString("time_firstgp_str"));
        PlayerPrefs.SetString("TimePlayinggp2", PlayerPrefs.GetString("time_firstgp_str"));
        PlayerPrefs.SetString("natP", "0");
        PlayerPrefs.SetString("QAnswered", "0");
        PlayerPrefs.SetString("statusPlay", "1");
        PlayerPrefs.SetString("statusAds", "0");
        gameDict = new Dictionary<string, string>();
        LoadDictionary("GoGreenDictionary", gameDict);

        if (!bsgame_audio_source.isPlaying)
        {
            //bsgame_audio_source = bsgame_object.GetComponent<AudioSource>();
            //bsgame_audio_source.GetComponent<AudioSource>().enabled = true;
            //bsgame_audio_source.Play();
        }

        if (!jawabanbenarsoundgame_audio_source.isPlaying)
        {
            jawabanbenarsoundgame_audio_source = jawabanbenarsoundgame_object.GetComponent<AudioSource>();
            jawabanbenarsoundgame_audio_source.GetComponent<AudioSource>().enabled = false;
            jawabanbenarsoundgame_audio_source.Stop();
        }

        if (!jawabansalahsoundgame_audio_source.isPlaying)
        {
            jawabansalahsoundgame_audio_source = jawabansalahsoundgame_object.GetComponent<AudioSource>();
            jawabansalahsoundgame_audio_source.GetComponent<AudioSource>().enabled = false;
            jawabansalahsoundgame_audio_source.Stop();
        }

        if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = false;
            bclickedsoundgame_audio_source.Stop();
        }
        StartCoroutine(checkInternetConnection((isConnected) => {
            if (!isConnected)
            {
                InternetLose.SetActive(true);
                MainDialogue.SetActive(false);
                GlobalVar.bannerView.Destroy();
            }
        }));
        
        
    }

    public float interval = 3f;
	float timer = 0;

    void Update()
    {
        timer -= Time.deltaTime;
        if(timer < 0)
		{
        
        StartCoroutine(loadAvatarEnemy(PlayerPrefs.GetString("bossgame_enemy_avatar")));

        timer = interval;
		}

        if(showGamePlay == true){
            if(GlobalVar.usersInGameLoaded == true){
                LoadingUserPage.SetActive(false);
                GamePlay.SetActive(true);

                ranQ(GlobalVar.isOwnerRoom);
                foreach (KeyValuePair<string, object> data in GlobalVar.dataUsersInRoom)
                {
                    string dataKey = data.Key.ToString();
                    if (dataKey.Contains(PlayerPrefs.GetString("bossgame_username")))
                    {
                        //Another User as Owner
                        //Debug.Log(data.Key);
                        LoadEnemyProfile(PlayerPrefs.GetString("bossgame_enemy_username"), PlayerPrefs.GetString("bossgame_enemy_total_natp"), PlayerPrefs.GetString("bossgame_enemy_heart"));
                        
                    }
                    else
                    {
                        //Another User as Guest
                        //Debug.Log(data.Key);
                        LoadEnemyProfile(PlayerPrefs.GetString("bossgame_enemy_username"), PlayerPrefs.GetString("bossgame_enemy_total_natp"), PlayerPrefs.GetString("bossgame_enemy_heart"));
                        
                    }
                }
                GlobalVar.usersInGameLoaded = false;
            } 
        }
        
        if (Input.GetKeyDown(KeyCode.Escape)) {
            //Debug.Log(PlayerPrefs.GetString("nickname"));
            StartCoroutine(postQuit(PlayerPrefs.GetString("nickname")));
            PlayerPrefs.SetString("time_end_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            PlayerPrefs.SetString("TimePlaying1", PlayerPrefs.GetString("time_first_str"));
            PlayerPrefs.SetString("TimePlaying2", PlayerPrefs.GetString("time_end_str"));
            StartCoroutine(postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), PlayerPrefs.GetString("bossgame_idgame"), PlayerPrefs.GetString("TimePlaying1"), PlayerPrefs.GetString("TimePlaying2")));
            Application.Quit();
        }
    }
    
    void displayImageAvatar(Texture2D imgToDisp)
    {
        //Resize Image
        //avatar.GetComponent<RectTransform>().sizeDelta = new Vector2(51, 51);
        //avatar.texture = imgToDisp;

        avatar2.GetComponent<RectTransform>().sizeDelta = new Vector2(51, 51);
        avatar2.texture = imgToDisp;
    }

    public void OnBackToHome()
    {
        Result rs = new Result();
        PlayerPrefs.SetString("statusAds", "0");
        randomizeQuestion = new int[gameDict.Count];
        System.Random randNum = new System.Random();
        for (int i = 0; i < randomizeQuestion.Length; i++)
        {
            randomizeQuestion[i] = randNum.Next(0, gameDict.Count);
        }
        SceneManager.LoadScene(0);
        PlayerPrefs.SetString("GoToMainMenu", "Yes");
        currentHangmanSprite = 0;
        currentQuest = 0;
        natP = 0;
        Qanswered = 0;
        PlayerPrefs.SetString("natP", "0");
        PlayerPrefs.SetString("QAnswered", "0");
        HangmanImage.sprite = Resources.Load<Sprite>("Sprites/" + currentHangmanSprite.ToString());

        if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
    }

    public void cancelToMainMenu()
    {
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
        Result rs = new Result();
        PlayerPrefs.SetString("statusAds", "0");
        SceneManager.LoadScene(0);
    }

    public void OnSinglePlayerClicked()
    {
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
        if(PlayerPrefs.GetString("statusPlay") == "0") {
            SceneManager.LoadScene(0);
        }
        else
        {
            randomizeQuestion = new int[gameDict.Count];
            System.Random randNum = new System.Random();
            for (int i = 0; i < randomizeQuestion.Length; i++)
            {
                randomizeQuestion[i] = randNum.Next(0, gameDict.Count);
            }
            MainDialogue.SetActive(true);
            currentHangmanSprite = 0;
            currentQuest = 0;
            natP = 0;
            Qanswered = 0;
            PlayerPrefs.SetString("natP", "0");
            PlayerPrefs.SetString("QAnswered", "0");
            HangmanImage.sprite = Resources.Load<Sprite>("Sprites/" + currentHangmanSprite.ToString());
            PickRandomQuestion();

            if (!bclickedsoundgame_audio_source.isPlaying)
            {
                bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
                bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
                bclickedsoundgame_audio_source.Play();
            }
        }
    }

    public void OnRestartClicked() {
        Login lg = new Login();
		int jumlahpoint = int.Parse(PlayerPrefs.GetString("bossgame_total_natp")) - int.Parse(PlayerPrefs.GetString("natP"));
		PlayerPrefs.SetString("bossgame_total_natp",jumlahpoint.ToString());
		TotalnaturepointBossgame2.text = "Your Nature Point : " + PlayerPrefs.GetString("bossgame_total_natp");
		if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
        randomizeQuestion = new int[gameDict.Count];
        System.Random randNum = new System.Random();
        for (int i = 0; i < randomizeQuestion.Length; i++)
        {
            randomizeQuestion[i] = randNum.Next(0, gameDict.Count);
        }
        MainDialogue.SetActive(true);
        currentHangmanSprite = 0;
        currentQuest = 0;
        natP = 0;
        Qanswered = 0;
        PlayerPrefs.SetString("natP", "0");
        PlayerPrefs.SetString("QAnswered", "0");
        HangmanImage.sprite = Resources.Load<Sprite>("Sprites/" + currentHangmanSprite.ToString());
        PickRandomQuestion();
    }

    public void OnGuessSubmitted(Button button) {
        Login lg = new Login();
        
        char letter = button.GetComponentInChildren<Text>().text.ToCharArray()[0];

        if (!bclickedsoundgame_audio_source.isPlaying)
        {
            bclickedsoundgame_audio_source = bclickedsoundgame_object.GetComponent<AudioSource>();
            bclickedsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            bclickedsoundgame_audio_source.Play();
        }
        if (answer.Contains(letter))
        {
            UpdateAnswerText(letter);
            if (CheckWinCondition())
            {
                //Debug.Log("You won the game !");
                currentQuest++;
                natP = natP + 10;
                Qanswered++;
				int jumlahpoint = int.Parse(PlayerPrefs.GetString("bossgame_total_natp")) + 10;
				PlayerPrefs.SetString("bossgame_total_natp",jumlahpoint.ToString());
				TotalnaturepointBossgame2.text = "Your Nature Point : " + PlayerPrefs.GetString("bossgame_total_natp");
                PlayerPrefs.SetString("natP", natP.ToString());
                PlayerPrefs.SetString("QAnswered", Qanswered.ToString());
                //Debug.Log("Test Score "+ Qanswered+" "+natP);
                if (currentQuest == gameDict.Count)
                {
                    ShowFinalDialogue(true);
                    //lg.time_endgp_str = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    //lg.TimePlayinggp[1] = lg.time_endgp_str;
                    //StartCoroutine(postNaturePointGame(lg.bossgame_iduser, lg.bossgame_idgame, lg.TimePlayinggp[0], lg.TimePlayinggp[1], natP.ToString()));
                    //StartCoroutine(postGetProfile(bossgame_nickname));
                }
                else
                {
                    PickRandomQuestion();
                }
            }
        }
        else
        {
            if (CheckLoseCondition())
            {
                ////Debug.Log("You lost the game");
                ShowFinalDialogue(false);
                //lg.time_endgp_str = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //lg.TimePlayinggp[1] = lg.time_endgp_str;
                //StartCoroutine(postNaturePointGame(lg.bossgame_iduser, lg.bossgame_idgame, lg.TimePlayinggp[0], lg.TimePlayinggp[1], natP.ToString()));
                //StartCoroutine(postGetProfile(bossgame_nickname));
                /*RequestRewardBasedVideo();
                if (reward_based_ad.IsLoaded())
                {
                    reward_based_ad.Show();
                }*/
            }
            else { DrawNextHangmanPart(); }
        } 
        
    }

    private void PickRandomQuestion()
    {
        Login lg = new Login();
        int randInt = randomizeQuestion[currentQuest];
        //int randInt = Random.Range(0, gameDict.Count);
        QuestionText.text = gameDict.ElementAt(randInt).Key;
        answer = gameDict.ElementAt(randInt).Value.ToUpper();
        StringBuilder sb = new StringBuilder("");
        ab1.SetActive(false);
        ab2.SetActive(false);
        ab3.SetActive(false);
        ab4.SetActive(false);
        ab5.SetActive(false);
        ab6.SetActive(false);
        ab7.SetActive(false);
        ab8.SetActive(false);
        ab9.SetActive(false);
        for (int i = 0; i < answer.Length; i++) {
            if(i % 3 == 0)
            {
                sb.Append(answer[i]);
                switch (i)
                {
                    case 0:
                        ab1.SetActive(true);
                        answerTextBox1.text = answer[i].ToString();
                        break;
                    case 1:
                        ab2.SetActive(true);
                        answerTextBox2.text = answer[i].ToString();
                        break;
                    case 2:
                        ab3.SetActive(true);
                        answerTextBox3.text = answer[i].ToString();
                        break;
                    case 3:
                        ab4.SetActive(true);
                        answerTextBox4.text = answer[i].ToString();
                        break;
                    case 4:
                        ab5.SetActive(true);
                        answerTextBox5.text = answer[i].ToString();
                        break;
                    case 5:
                        ab6.SetActive(true);
                        answerTextBox6.text = answer[i].ToString();
                        break;
                    case 6:
                        ab7.SetActive(true);
                        answerTextBox7.text = answer[i].ToString();
                        break;
                    case 7:
                        ab8.SetActive(true);
                        answerTextBox8.text = answer[i].ToString();
                        break;
                    case 8:
                        ab9.SetActive(true);
                        answerTextBox9.text = answer[i].ToString();
                        break;
                }
            }
            else
            {
                sb.Append(PLACEHOLDER);
                switch (i)
                {
                    case 0:
                        ab1.SetActive(true);
                        answerTextBox1.text = PLACEHOLDER.ToString();
                        break;
                    case 1:
                        ab2.SetActive(true);
                        answerTextBox2.text = PLACEHOLDER.ToString();
                        break;
                    case 2:
                        ab3.SetActive(true);
                        answerTextBox3.text = PLACEHOLDER.ToString();
                        break;
                    case 3:
                        ab4.SetActive(true);
                        answerTextBox4.text = PLACEHOLDER.ToString();
                        break;
                    case 4:
                        ab5.SetActive(true);
                        answerTextBox5.text = PLACEHOLDER.ToString();
                        break;
                    case 5:
                        ab6.SetActive(true);
                        answerTextBox6.text = PLACEHOLDER.ToString();
                        break;
                    case 6:
                        ab7.SetActive(true);
                        answerTextBox7.text = PLACEHOLDER.ToString();
                        break;
                    case 7:
                        ab8.SetActive(true);
                        answerTextBox8.text = PLACEHOLDER.ToString();
                        break;
                    case 8:
                        ab9.SetActive(true);
                        answerTextBox9.text = PLACEHOLDER.ToString();
                        break;
                }
            } 
        }
        DashesText.text = sb.ToString();
        userInput = sb.ToString();
        PlayerPrefs.SetString("time_firstgp_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        PlayerPrefs.SetString("time_endgp_str", "");
        PlayerPrefs.SetString("TimePlayinggp1", PlayerPrefs.GetString("time_firstgp_str"));
        PlayerPrefs.SetString("TimePlayinggp2", PlayerPrefs.GetString("time_firstgp_str"));
        //StartCoroutine(postGetProfile(PlayerPrefs.GetString("nickname")));
        ////Debug.Log("CQu"+ currentQuest + "Question" + gameDict.ElementAt(randInt).Key + "Answer: " + answer);
    }

    private void LoadDictionary(string dictFileName, Dictionary<string, string> outputDict)
    {
        TextAsset ta = Resources.Load(dictFileName) as TextAsset;
        JSONObject jsonObj = (JSONObject)JSON.Parse(ta.text);
        foreach (var key in jsonObj.GetKeys()) { outputDict[key] = jsonObj[key]; }
    }

    private void UpdateAnswerText(char letter) {
        if (currentQuest == gameDict.Count)
        { 
        }
        else { 
            if (!jawabanbenarsoundgame_audio_source.isPlaying)
            {
                jawabanbenarsoundgame_audio_source = jawabanbenarsoundgame_object.GetComponent<AudioSource>();
                jawabanbenarsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
                jawabanbenarsoundgame_audio_source.Play();
            }
        }
        char[] userInputArray = userInput.ToCharArray();
        for (int i = 0; i < answer.Length; i++) {
            if (userInputArray[i] != PLACEHOLDER) { continue; } // already guessed
            if (answer[i] == letter) { 
                userInputArray[i] = letter;
                switch (i)
                {
                    case 0:
                        answerTextBox1.text = answer[i].ToString();
                        break;
                    case 1:
                        answerTextBox2.text = answer[i].ToString();
                        break;
                    case 2:
                        answerTextBox3.text = answer[i].ToString();
                        break;
                    case 3:
                        answerTextBox4.text = answer[i].ToString();
                        break;
                    case 4:
                        answerTextBox5.text = answer[i].ToString();
                        break;
                    case 5:
                        answerTextBox6.text = answer[i].ToString();
                        break;
                    case 6:
                        answerTextBox7.text = answer[i].ToString();
                        break;
                    case 7:
                        answerTextBox8.text = answer[i].ToString();
                        break;
                    case 8:
                        answerTextBox9.text = answer[i].ToString();
                        break;
                }
            }
        }
        userInput = new string(userInputArray);
        DashesText.text = userInput;
    }

    private void DrawNextHangmanPart() {
        if (!jawabansalahsoundgame_audio_source.isPlaying)
        {
            jawabansalahsoundgame_audio_source = jawabansalahsoundgame_object.GetComponent<AudioSource>();
            jawabansalahsoundgame_audio_source.GetComponent<AudioSource>().enabled = true;
            jawabansalahsoundgame_audio_source.Play();
        }
        currentHangmanSprite = ++currentHangmanSprite % TOTAL_HANGMAN_SPRITES;
        HangmanImage.sprite = Resources.Load<Sprite>("Sprites/"+ currentHangmanSprite.ToString());
    }

    private bool CheckWinCondition() { return answer.Equals(userInput); }
    private bool CheckLoseCondition() { return currentHangmanSprite == TOTAL_HANGMAN_SPRITES-1; }
    
    private void ShowFinalDialogue(bool win) {
        Result rs = new Result();
        //Debug.Log(natP.ToString() + " " + Qanswered.ToString());
        if (win)
        {
            GlobalVar.bannerView.Destroy();
            PlayerPrefs.SetString("openPageResult", "0");
            SceneManager.LoadScene(2);
        }
        else
        {
            GlobalVar.bannerView.Destroy();
            PlayerPrefs.SetString("openPageResult", "1");
            SceneManager.LoadScene(2);
        }
    }
}
