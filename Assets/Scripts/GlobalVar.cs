﻿using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;


public class GlobalVar {
    public static BannerView bannerView;
	public static string version_game="1.0.9";
	
    public static Dictionary<string, object> dataUsersInRoom;
    public static bool usersInGameLoaded = false;
    public static bool isOwnerRoom = true;
}
