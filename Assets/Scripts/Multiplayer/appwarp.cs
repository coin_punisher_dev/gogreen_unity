using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using com.shephertz.app42.gaming.multiplayer.client;
using com.shephertz.app42.gaming.multiplayer.client.events;

using SimpleJSON;

//using UnityEditor;

using AssemblyCSharp;

public class appwarp : MonoBehaviour {

	//please update with values you get after signing up
	public static string apiKey = "f5882a001cef5afc6ce00679e563af67a47970323ca98f2d82634f56420c10b7";
	public static string secretKey = "f7a6376b57132fe3f790b997fd1cc0c21212a27c5cc5e60efbdcdcae5a97821e";
	public static string roomid = "1193068218";
	public static string username;
	public Listener listener;
	public static Vector3 newPos = new Vector3(0,0,0);

    private Dictionary<string, object> data;
    private List<string> rooms;
    private int index = 0;
    

    void Start () {
		listener = new Listener();
		WarpClient.initialize(apiKey,secretKey);
		WarpClient.GetInstance().AddConnectionRequestListener(listener);
        WarpClient.GetInstance().AddChatRequestListener(listener);
        WarpClient.GetInstance().AddUpdateRequestListener(listener);
        WarpClient.GetInstance().AddLobbyRequestListener(listener);
        WarpClient.GetInstance().AddNotificationListener(listener);
        WarpClient.GetInstance().AddRoomRequestListener(listener);
        WarpClient.GetInstance().AddZoneRequestListener(listener);
        WarpClient.GetInstance().AddTurnBasedRoomRequestListener(listener);
		// join with a unique name (current time stamp)
		//username = System.DateTime.UtcNow.Ticks.ToString();
		username = PlayerPrefs.GetString("bossgame_username");
		WarpClient.GetInstance().Connect(username);
		
		//EditorApplication.playmodeStateChanged += OnEditorStateChanged;
		
	}

	private void OnEnable() {
        Listener.OnConnect += OnConnectOccured;
        Listener.OnRoomsInRange += OnRoomsInRangeOccured;
        Listener.OnCreateRoom += OnCreateRoomOccured;
        Listener.OnGetLiveRoomInfo += OnGetLiveRoomInfoOccured;
        Listener.OnUserJoinRoom += OnUserJoinRoomOccured;
    }

    private void OnDisable() {
        Listener.OnConnect -= OnConnectOccured;
        Listener.OnRoomsInRange -= OnRoomsInRangeOccured;
        Listener.OnCreateRoom -= OnCreateRoomOccured;
        Listener.OnGetLiveRoomInfo -= OnGetLiveRoomInfoOccured;
        Listener.OnUserJoinRoom -= OnUserJoinRoomOccured;
    }

	public void OnConnectOccured(bool _IsSuccess) {
        Debug.Log("OnConnect: " + _IsSuccess);
        WarpClient.GetInstance().GetRoomsInRange(1, 2);
    }

    public void OnRoomsInRangeOccured(bool _IsSuccess, MatchedRoomsEvent eventObj) {
        Debug.Log("OnRoomsInRange: " + _IsSuccess + " " + eventObj.getRoomsData());
        if(_IsSuccess) {
            rooms = new List<string>();
            foreach(var roomData in eventObj.getRoomsData()) {
                Debug.Log("Getting Live info on room " + roomData.getId());
                Debug.Log("Room Owner " + roomData.getRoomOwner());
                rooms.Add(roomData.getId());
            }

            index = 0;
            if(index < rooms.Count) {
                Debug.Log("Getting Live Info on room: " + rooms[index]);
                WarpClient.GetInstance().GetLiveRoomInfo(rooms[index]);
            }
            else {
                Debug.Log("No rooms were availible, create a room");
                WarpClient.GetInstance().CreateRoom(username, username, 2, data);
                //WarpClient.GetInstance().CreateTurnRoom(username+" room", username, 2, data, 30);
            }
        }
        else {
            Debug.Log("OnRoomsInRangeOccured: connection failed!");
        }
    }

    public void OnCreateRoomOccured(bool _IsSuccess, string _RoomId) {
        Debug.Log("OnCreateRoom " + _IsSuccess + " " + _RoomId);
        
        if (_IsSuccess)
        {

            WarpClient.GetInstance().JoinRoom(_RoomId);
            WarpClient.GetInstance().SubscribeRoom(_RoomId);
            data = new Dictionary<string, object> {
                { username+"_avatar", PlayerPrefs.GetString("bossgame_avatar") },
                { username+"_username", PlayerPrefs.GetString("bossgame_username") },
                { username+"_health", PlayerPrefs.GetString("bossgame_heart") },
                { username+"_nature_point", PlayerPrefs.GetString("bossgame_total_natp") }
            };
            var listRemove = new List<string>(data.Keys);
            WarpClient.GetInstance().UpdateRoomProperties(_RoomId, null, listRemove);
            WarpClient.GetInstance().UpdateRoomProperties(_RoomId, data, null);
            WarpClient.GetInstance().GetOnlineUsers();
        }
    }

    public void OnGetLiveRoomInfoOccured(LiveRoomInfoEvent eventObj) {
        Debug.Log("OnGetLiveRoomInfo " + eventObj.getData().getId() + " " + eventObj.getResult() + " " + eventObj.getJoinedUsers().Length);
        if(eventObj.getResult() == 0 && eventObj.getJoinedUsers().Length == 1){
            
            WarpClient.GetInstance().JoinRoom(eventObj.getData().getId());
            WarpClient.GetInstance().SubscribeRoom(eventObj.getData().getId());
            data = new Dictionary<string, object> {
                { username+"_avatar", PlayerPrefs.GetString("bossgame_avatar") },
                { username+"_username", PlayerPrefs.GetString("bossgame_username") },
                { username+"_health", PlayerPrefs.GetString("bossgame_heart") },
                { username+"_nature_point", PlayerPrefs.GetString("bossgame_total_natp") }
            };
            var listRemove = new List<string>(data.Keys);
            WarpClient.GetInstance().UpdateRoomProperties(eventObj.getData().getId(), null, listRemove);
            WarpClient.GetInstance().UpdateRoomProperties(eventObj.getData().getId(), data, null);
            WarpClient.GetInstance().GetOnlineUsers();
            GlobalVar.usersInGameLoaded = true;
            GlobalVar.isOwnerRoom = false;
        }
        //WarpClient.GetInstance().SendChat(msg);
    }

    public void OnUserJoinRoomOccured(RoomData eventObj, string _UserName) {
        Debug.Log("OnUserJoinRoom " + " " + _UserName);
        GlobalVar.usersInGameLoaded = true;
        GlobalVar.isOwnerRoom = true;
    }

	
	public float interval = 0.5f;
	float timer = 0;
	
	void Update () {
		timer -= Time.deltaTime;
		if(timer < 0)
		{
            timer = interval;
		}
		
		if (Input.GetKeyDown(KeyCode.Escape)) {
        	Application.Quit();
    	}
		WarpClient.GetInstance().Update();
	}
	
	void OnGUI()
	{
	}
	
	/*void OnEditorStateChanged()
	{
    	if(EditorApplication.isPlaying == false) 
		{
			WarpClient.GetInstance().Disconnect();
    	}
	}*/
	
	void OnApplicationQuit()
	{
		WarpClient.GetInstance().Disconnect();
	}
	
}