﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GoogleMobileAds.Api;

public class GoogleAds : MonoBehaviour
{
    public void RequestBanner()
    {
        #if UNITY_ANDROID
			//Testing
            //string adUnitId = "ca-app-pub-3940256099942544/6300978111";
			//Production
            string adUnitId = "ca-app-pub-9972612069234436/1574712379";
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
        #else
            string adUnitId = "unexpected_platform";
        #endif

        // Create a 320x50 banner at the Bottom of the screen.
        AdSize adSize = new AdSize(320, 50);
        GlobalVar.bannerView = new BannerView(adUnitId, adSize, AdPosition.Bottom);

        // Called when an ad request has successfully loaded.
        GlobalVar.bannerView.OnAdLoaded += this.HandleOnAdLoaded;
        // Called when an ad request failed to load.
        GlobalVar.bannerView.OnAdFailedToLoad += this.HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        GlobalVar.bannerView.OnAdOpening += this.HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        GlobalVar.bannerView.OnAdClosed += this.HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        GlobalVar.bannerView.OnAdLeavingApplication += this.HandleOnAdLeavingApplication;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        GlobalVar.bannerView.LoadAd(request);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	public void startBanner(){
		#if UNITY_ANDROID
			//Testing
            //string appId = "ca-app-pub-3940256099942544~3347511713";
			//Production
            string appId = "ca-app-pub-9972612069234436~5411204971";
        #elif UNITY_IPHONE
            string appId = "ca-app-pub-3940256099942544~1458002511";
        #else
            string appId = "unexpected_platform";
        #endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
		this.RequestBanner();
	}
	
	public void destoryBanner(){
		GlobalVar.bannerView.Destroy();
	}
	
	public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleOnAdLoaded", 1, 0.0));
        MonoBehaviour.print("HandleOnAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleOnAdFailedToLoad event received with message: " + args.Message);
        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleOnAdFailedToLoad", 2, 0.0));
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleOnAdOpened event received");
        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleOnAdOpened", 3, 0.0));
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleOnAdClosed event received");
        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleOnAdClosed", 5, 0.0));
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleOnAdLeavingApplication event received");

        MainScript ms = new MainScript();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("nickname"), "HandleOnAdLeavingApplication", 7, 0.0));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
