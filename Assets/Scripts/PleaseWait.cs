﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PleaseWait : MonoBehaviour
{
    public Text PleaseWaitGameObject;
    private string story;

    IEnumerator TypingText()
	{
		foreach (char c in this.story) 
		{
			PleaseWaitGameObject.text += c;
			yield return new WaitForSeconds (0.5f);
		}
	}

    // Start is called before the first frame update
    void Start()
    {
        this.story = PleaseWaitGameObject.text;
        PleaseWaitGameObject.text = "";
    }

    /// <summary>
    /// OnGUI is called for rendering and handling GUI events.
    /// This function can be called multiple times per frame (one call per event).
    /// </summary>
    void OnGUI()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(PleaseWaitGameObject.text=="Please Wait ..."){
            this.story = PleaseWaitGameObject.text;
            PleaseWaitGameObject.text = ""; 
        }else if(PleaseWaitGameObject.text==""){
            StartCoroutine ("TypingText");
        } 
    }
}
